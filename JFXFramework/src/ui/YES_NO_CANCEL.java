package ui;

/**
 * Created by Yixiu Liu on 10/14/2016.
 * @author Yixiu Liu
 */
public enum YES_NO_CANCEL {
    YES, NO, CANCEL
}
