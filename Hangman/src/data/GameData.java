package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * @author Ritwik Banerjee, Yixiu Liu
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;
    private static final Pattern ILLEGAL_CHARACTERS = Pattern.compile("[^A-Za-z]");

    private String         targetWord;
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private int            remainingGuesses;
    private boolean        hintAvailable;
    public  AppTemplate    appTemplate;

    public GameData(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        appTemplate.setDataComponent(this);
        reset();
    }

    @Override
    public void reset() {
        //this.targetWord = null;
        this.targetWord = setTargetWord();
        while(notValidWord(targetWord)){
            this.targetWord = setTargetWord();
        }

        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        this.hintAvailable = !isEasy();
    }

    public String getTargetWord() {
        return targetWord;
    }

    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            ArrayList<String> wordList = new ArrayList<>(TOTAL_NUMBER_OF_STORED_WORDS);

            for(Iterator<String> i = lines.iterator(); i.hasNext();){
                String word = i.next();
                if(!notValidWord(word) && !"".equals(word)){
                    wordList.add(word);
                }
            }

            if(wordList.size() <= 0)
                throw new IllegalArgumentException();

            int toSkip = new Random().nextInt(wordList.size());
            return wordList.get(toSkip);

            /*
            int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);
            return lines.skip(toSkip).findFirst().get().toLowerCase();
            */
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public GameData setTargetWord(String targetWord) throws IOException {
        if(notValidWord(targetWord))
            throw new IOException();

        this.targetWord = targetWord;
        return this;
    }

    private boolean notValidWord(String word){
        return ILLEGAL_CHARACTERS.matcher(word).find();
    }

    public boolean isEasy(){
        int distinct = 0;
        HashSet<Character> list = new HashSet<>();

        for(char c: targetWord.toCharArray()){
            if(!list.contains(c)){
                list.add(c);
                distinct++;
            }
        }
        return distinct<=7;
    }

    public boolean isHintAvailable(){
        return hintAvailable;
    }

    public void setHintAvailable(boolean available){
        hintAvailable = available;
    }

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    @SuppressWarnings("unused")
    public GameData setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
        return this;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    @SuppressWarnings("unused")
    public GameData setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
        return this;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void setRemainingGuesses(int remainingGuesses){ this.remainingGuesses = remainingGuesses; }

    public void addGoodGuess(char c) {
        goodGuesses.add(c);
    }

    public void addBadGuess(char c) {
        if (!badGuesses.contains(c)) {
            badGuesses.add(c);
            remainingGuesses--;
        }
    }


}
