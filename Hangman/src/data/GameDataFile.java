package data;

import com.fasterxml.jackson.databind.ObjectMapper;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

/**
 * @author Ritwik Banerjee, Yixiu Liu
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";

    @Override
    public void saveData(AppDataComponent data, Path to) throws IOException{
        SavableData savableData = new SavableData((GameData) data);
        ObjectMapper mapper = new ObjectMapper();

        //System.out.println(mapper.writeValueAsString(savableData));
        mapper.writerWithDefaultPrettyPrinter().writeValue(to.toFile(), savableData);
    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        GameData dataToMod = (GameData) data;
        ObjectMapper mapper = new ObjectMapper();
        SavableData loadedData = mapper.readValue(from.toFile(), SavableData.class);

        dataToMod.setTargetWord(loadedData.getTargetWord()); //Target word must be set first for special char  validity check
        dataToMod.setBadGuesses(loadedData.getBadGuesses());
        dataToMod.setGoodGuesses(loadedData.getGoodGuesses());
        dataToMod.setRemainingGuesses(loadedData.getRemainingGuesses());
        dataToMod.setHintAvailable(loadedData.getHintAvailable());
    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }

    private static class SavableData {
        private String targetWord;
        private Set<Character> goodGuesses;
        private Set<Character> badGuesses;
        private int remainingGuesses;
        private boolean hintAvailable;

        private SavableData(){

        }

        private SavableData(GameData data){
            this(data.getTargetWord(),
                    data.getGoodGuesses(),
                    data.getBadGuesses(),
                    data.getRemainingGuesses(),
                    data.isHintAvailable()
            );
        }

        private SavableData(String targetWord, Set<Character> goodGuesses, Set<Character> badGuesses, int remainingGuesses, boolean hintAvailable){
            this.targetWord = targetWord;
            this.goodGuesses = goodGuesses;
            this.badGuesses = badGuesses;
            this.remainingGuesses = remainingGuesses;
            this.hintAvailable = hintAvailable;
        }

        public void setTargetWord(String targetWord) {
            this.targetWord = targetWord;
        }

        public void setGoodGuesses(Set<Character> goodGuesses) {
            this.goodGuesses = goodGuesses;
        }

        public void setBadGuesses(Set<Character> badGuesses) {
            this.badGuesses = badGuesses;
        }

        public void setRemainingGuesses(int remainingGuesses) {
            this.remainingGuesses = remainingGuesses;
        }

        public void setHintAvailable(boolean hintAvailable){
            this.hintAvailable = hintAvailable;
        }

        public String getTargetWord() {
            return targetWord;
        }

        public Set<Character> getGoodGuesses() {
            return goodGuesses;
        }

        public Set<Character> getBadGuesses() {
            return badGuesses;
        }

        public int getRemainingGuesses() {
            return remainingGuesses;
        }

        public boolean getHintAvailable(){ return hintAvailable; }
    }
}
