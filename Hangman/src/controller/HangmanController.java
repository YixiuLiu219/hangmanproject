package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YES_NO_CANCEL;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee, Yixiu Liu
 */
public class HangmanController implements FileController {

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private Text[]      progress;    // reference to the text area for the word
    private Rectangle[] usedLetterPads = new Rectangle[26];
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered (repeats count as multiples)
    private Button      gameButton;  // shared reference to the "start game" button
    private Button      hintButton;
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private boolean     gameover;    // whether or not the current game is already over
    private boolean     savable;
    private Path        workFile;
    private AnimationTimer runningGame;
    private final Pattern ILLEGAL_KEYS = Pattern.compile("[^A-Za-z]");

    private final Text winText = new Text("YOU WIN!");
    private final Text loseText = new Text("YOU LOSE!");
    private final Font resultFont = new Font(50);
    private final Color resultColor = Color.VIOLET;

    private final Color correctLetterPadColor = Color.DARKGRAY;
    private final Font correctLetterFont = new Font(40);

    private final Color alreadyGuessedPadColor = Color.DARKCYAN;
    private final Color keyboardColor = Color.CYAN;
    private final Font alreadyGuessedFont = new Font(20);
    private final Color alreadyGuessedColor = Color.LIGHTBLUE;

    private final Color loseLetterColor = Color.RED;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
        gamedata = (GameData) appTemplate.getDataComponent();
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        gamedata = (GameData) appTemplate.getDataComponent();
    }

    private void createKeyboard(Collection<String> list){
        Workspace ws = (Workspace)appTemplate.getWorkspaceComponent();
        Pane pane = ws.getAlreadyGuessedBox();

        for(int i=0; i<usedLetterPads.length; i++){

            Text text  = new Text(Character.toString((char)(i+'a')));
            text.setFont(alreadyGuessedFont);
            text.setFill(alreadyGuessedColor);

            usedLetterPads[i] = ws.makePad(text, keyboardColor, new StackPane(), pane);
        }

        for(String s: list){
            if(Character.isAlphabetic(s.charAt(0))){
                usedLetterPads[Character.toLowerCase(s.charAt(0))-'a'].setFill(alreadyGuessedPadColor);
            }
        }
    }

    private void updateKeyboard(char c) {
        if (Character.isAlphabetic(c)) {usedLetterPads[Character.toLowerCase(c) - 'a'].setFill(alreadyGuessedPadColor);}
    }

    /**
     * This method ensures start button reference in controller
     * and enables the start button.
     */
    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    /**
     * This method is called by any method that creates or reloads workspace
     * to ensure that Controller has proper reference to the start button.
     */
    public void ensureGameButton(){
        if(gameButton == null)
            gameButton = ((Workspace) appTemplate.getWorkspaceComponent()).getStartGame();
    }

    public void start() {
        //gamedata = new GameData(appTemplate);
        //if(gamedata == null)
            //gamedata = (GameData) appTemplate.getDataComponent();
        //else
        //if(runningGame != null) runningGame.stop(); //not used due to disabling button
        gamedata.reset();
        appTemplate.getWorkspaceComponent().reloadWorkspace();

        gameover = false;
        success = false;
        savable = true;
        discovered = 0;
        char[] word = gamedata.getTargetWord().toCharArray();
        for (int i = 0; i < word.length; i++)
            if(gamedata.getGoodGuesses().contains(word[i]))
                discovered++;


        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.getStartGame().setDisable(true);
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox letterPane    = gameWorkspace.getLetterPane();

        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(letterPane);

        //already guessed
        //HBox alreadyGuessesBox = gameWorkspace.getAlreadyGuessedBox();
        //Text alreadyGuessesTitle = new Text("Guessed Letters: ");
        //alreadyGuessesTitle.setFont(alreadyGuessedFont);
        //alreadyGuessesBox.getChildren().add(alreadyGuessesTitle);
        createKeyboard(new ArrayList<>());

        //hint button
        if(!gamedata.isEasy()) {
            hintButton = new Button("HINT");
            hintButton.setDisable(!gamedata.isHintAvailable());
            hintButton.setOnMouseClicked(e -> showHint());
            gameWorkspace.getGameTextsPane().getChildren().add(hintButton);
        }
        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null); //Listener destroyed
        gameover = true;
        gameButton.setDisable(true);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

        //Update Hint
        disableHint();
        //gamedata.setHintAvailable(false);
    }

    /**
     * This method initializes all the letters to invisible
     * @param guessedLetters
     */
    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);

            progress[i].setFont(correctLetterFont);
            guessedLetters.getChildren().add(((Workspace)appTemplate.getWorkspaceComponent()).makePad(progress[i], correctLetterPadColor));
        }
        //guessedLetters.getChildren().addAll(progress);
    }

    /**
     * This method initializes all the letters to invisible only if it is not
     * contained in the GoodGuesses set of gamdata.
     * @param guessedLetters
     */
    private void considerateInitWordGraphics(HBox guessedLetters){
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(gamedata.getGoodGuesses().contains(targetword[i]));

            progress[i].setFont(correctLetterFont);
            guessedLetters.getChildren().add(((Workspace)appTemplate.getWorkspaceComponent()).makePad(progress[i], correctLetterPadColor));
        }
        //guessedLetters.getChildren().addAll(progress);

        //hangman graphics update
        Workspace ws = (Workspace) appTemplate.getWorkspaceComponent();
        ws.updateHangmanGraphics(gamedata.getBadGuesses().size());
    }

    public void play() {//Play is in its own thread, if play() is invoked twice, then two threads are running on same data
        runningGame = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {

                    //turn letter to lower-case, if it is not letter, it is checked by illegalKey(char)
                    char input = event.getCharacter().charAt(0);
                    input = Character.isAlphabetic(input) ? Character.toLowerCase(input) : input;

                    char guess = input;
                    if (!illegalKey(guess) && !alreadyGuessed(guess)) {
                        boolean goodguess = false;
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {
                                progress[i].setVisible(true);
                                gamedata.addGoodGuess(guess);
                                goodguess = true;
                                discovered++;
                            }
                        }
                        if (!goodguess) {
                            gamedata.addBadGuess(guess);
                        }

                        //Update used guesses graphics
                        //update hangman graphics
                        Platform.runLater( ()-> {
                            //updateGuessedGraphics(guess);
                            if(gamedata.getRemainingGuesses() <= 1 || discovered>=progress.length-1){
                                disableHint();
                                gamedata.setHintAvailable(false);
                            }
                            updateKeyboard(guess);
                            Workspace ws = (Workspace) appTemplate.getWorkspaceComponent();
                            ws.updateHangmanGraphics(gamedata.getBadGuesses().size());
                        });

                        //success = (discovered == progress.length);
                        //remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    }
                });

                success = (discovered == progress.length);
                remains.setText(Integer.toString(gamedata.getRemainingGuesses()));

                if (gamedata.getRemainingGuesses() <= 0 || success) {
                    stop();
                    savable = false;    //cannot save game if it is over by natural end (not when interrupted by killGameProcess()

                    //Update natural end game word graphics
                    Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                    for(int i=0; i<progress.length; i++){
                        if(!progress[i].isVisible()){
                            progress[i].setVisible(true);
                            progress[i].setFill(loseLetterColor);
                        }
                    }

                    //Win or Lose message
                    winText.setFont(resultFont);
                    loseText.setFont(resultFont);
                    winText.setFill(resultColor);
                    loseText.setFill(resultColor);
                    gameWorkspace.getGameTextsPane().getChildren().add(success? winText : loseText);

                    //Display Message
                    System.out.println(success ? "You win!" : "Ah, close but not quite there. The word was \"" + gamedata.getTargetWord() + "\".");
                    //Removed due to gui already doing so
                    //Platform.runLater(()-> {
                    //    AppMessageDialogSingleton.getSingleton().show("", success ? "You win!" : "The word was \"" + gamedata.getTargetWord() + "\".");
                    //});
                }
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };

        runningGame.start();
    }

    private void showHint(){
        ArrayList<Character> list = new ArrayList<>();
        for(char c: gamedata.getTargetWord().toCharArray()){
            if(!gamedata.getGoodGuesses().contains(c) && !list.contains(c)){
                list.add(c);
            }
        }

        int randInt = new Random().nextInt(list.size());
        char guess = list.get(randInt);

        //progress
        for (int i = 0; i < progress.length; i++) {
            if (gamedata.getTargetWord().charAt(i) == guess) {
                progress[i].setVisible(true);
                gamedata.addGoodGuess(guess);
                discovered++;
            }
        }

        //update graphics
        updateGuessedGraphics(guess);
        Workspace ws = (Workspace) appTemplate.getWorkspaceComponent();
        ws.updateHangmanGraphics(gamedata.getBadGuesses().size());

        gamedata.setRemainingGuesses(gamedata.getRemainingGuesses()-1);
        gamedata.setHintAvailable(false);

        disableHint();
    }

    private void disableHint(){
        if(hintButton != null)
            hintButton.setDisable(true);
    }

    private void updateGuessedGraphics(Character...chars){
        for(char c: chars) {
            Text newLetter = new Text(Character.toString(c));
            newLetter.setFont(alreadyGuessedFont);
            newLetter.setFill(alreadyGuessedColor);
            Workspace ws = (Workspace) appTemplate.getWorkspaceComponent();
            ws.getAlreadyGuessedBox().getChildren().add(ws.makePad(newLetter, alreadyGuessedPadColor));
        }
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    /**
     * Terminates the on-going game thread.
     */
    private void killGameProcess(){
        if(runningGame!=null)
            runningGame.stop();
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

        if (savable) {
            try {
                //promptToSave();
                switch(promptToSave()){
                    case YES: handleSaveRequest(); break;
                    case CANCEL: makenew = false; gameover = false; break;
                    default: //do nothing
                }
            } catch (IOException e) {
                return;
                //messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        }
        if (makenew) {
            killGameProcess();
            //savable = false;
            //appTemplate.getGUI().updateWorkspaceToolbar(savable);
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

        if (gameover) {
            killGameProcess();
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }
    
    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if(workFile != null){
            appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), workFile);

            //Display message
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_WORK_TITLE), propertyManager.getPropertyValue(SAVE_COMPLETED_MESSAGE));
        }
        else{
            //Initialize FileChooser
            FileChooser chooser = new FileChooser();
            chooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            chooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(
                    String.format("%s (*.%s)", propertyManager.getPropertyValue(WORK_FILE_EXT_DESC), propertyManager.getPropertyValue(WORK_FILE_EXT)),
                    String.format("*.%s", propertyManager.getPropertyValue(WORK_FILE_EXT))));

            //Choose a initial directory
            try {
                File file = new File(System.getProperty("user.dir")+"/Hangman/resources/saved");
                chooser.setInitialDirectory(file);
            }catch (NullPointerException e){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE),
                        APP_WORKDIR_PATH.getParameter()+" directory not found under resources. FileChooser will launch with default directory.");
            }

            //Make the file
            File saveFile = chooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if(saveFile != null) {

                //Ensure extension
                try{
                    String name = saveFile.toString();
                    if(!name.substring(name.lastIndexOf(".")+1).equals(propertyManager.getPropertyValue(WORK_FILE_EXT))) {
                        //name = name.substring(0, name.lastIndexOf(".")+1) + propertyManager.getPropertyValue(WORK_FILE_EXT);
                        name += "."+propertyManager.getPropertyValue(WORK_FILE_EXT);
                        saveFile = new File(name);
                    }
                }catch(IndexOutOfBoundsException e){
                    saveFile = new File(saveFile +"."+propertyManager.getPropertyValue(WORK_FILE_EXT));
                }

                Path path = saveFile.toPath();
                appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), path);
                workFile = path;

                //Display message
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(propertyManager.getPropertyValue(SAVE_WORK_TITLE), propertyManager.getPropertyValue(SAVE_COMPLETED_MESSAGE));
            }
            else{
                throw new IOException();//If user canceled the save, throw exception as flag so the current game continues.
            }
        }
    }

    @Override
    public void handleLoadRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        FileChooser chooser = new FileChooser();
        chooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
        chooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(
                String.format("%s (*.%s)", propertyManager.getPropertyValue(WORK_FILE_EXT_DESC), propertyManager.getPropertyValue(WORK_FILE_EXT)),
                String.format("*.%s", propertyManager.getPropertyValue(WORK_FILE_EXT))));

        //Choose a initial directory
        try {
            File file = new File(System.getProperty("user.dir")+"/Hangman/resources/saved");// /Hangman/resource/saved
            chooser.setInitialDirectory(file);
        }catch (NullPointerException e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(LOAD_ERROR_TITLE), "Cannot find Work Directory: "
                    +APP_WORKDIR_PATH.getParameter()+". FileChooser will launch with default directory.");
        }

        //Load file
        File loadedFile = chooser.showOpenDialog(appTemplate.getGUI().getWindow());

        if(loadedFile != null) {
            if(!loadedFile.toString().substring(loadedFile.toString().lastIndexOf(".")+1).equals(propertyManager.getPropertyValue(WORK_FILE_EXT)))
                throw new IOException();

            workFile = loadedFile.toPath();
            appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), loadedFile.toPath());

            //Consider making below into offical start-game method.
            //Update GUI
            killGameProcess();
            gameover = false;
            success = false;
            savable = true;
            discovered = 0;
            char[] word = gamedata.getTargetWord().toCharArray();
            for (int i = 0; i < word.length; i++)
                if (gamedata.getGoodGuesses().contains(word[i]))
                    discovered++;

            ensureGameButton();
            ensureActivatedWorkspace();
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reloadWorkspace();
            gameWorkspace.getStartGame().setDisable(true);
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
            HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

            remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
            remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
            considerateInitWordGraphics(guessedLetters);

            //update guess graphics
            //HBox alreadyGuessedBox = gameWorkspace.getAlreadyGuessedBox();
            createKeyboard(new LinkedList<>());
            for(Character c: gamedata.getBadGuesses())
                updateKeyboard(c);
            for(Character c: gamedata.getGoodGuesses())
                updateKeyboard(c);
            /*
            Text alreadyGuessedTitle = new Text("Guessed Letters: ");
            alreadyGuessedTitle.setFont(alreadyGuessedFont);
            alreadyGuessedBox.getChildren().setAll(alreadyGuessedTitle);
            for(Character c: gamedata.getBadGuesses())
                updateGuessedGraphics(c);
            for(Character c: gamedata.getGoodGuesses())
                updateGuessedGraphics(c);
                */

            //hint button
            if(!gamedata.isEasy()) {
                hintButton = new Button("HINT");
                hintButton.setDisable(!gamedata.isHintAvailable());
                hintButton.setOnMouseClicked(e -> showHint());
                gameWorkspace.getGameTextsPane().getChildren().add(hintButton);
            }

            play();

            //Display message
            AppMessageDialogSingleton.getSingleton().show(
                    propertyManager.getPropertyValue(LOAD_COMPLETED_TITLE), propertyManager.getPropertyValue(LOAD_COMPLETED_MESSAGE));
        }
    }
    
    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (savable) {
                //exit = promptToSave();
                switch(promptToSave()) {
                    case YES: handleSaveRequest(); break;
                    case CANCEL: exit = false; break;
                    default: //exit
                }
            }
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            //do nothing
            //AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            //PropertyManager           props  = PropertyManager.getManager();
            //dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }
    
    private YES_NO_CANCEL promptToSave() throws IOException {
        //return false; // dummy placeholder
        ///*
        PropertyManager propertyManager = PropertyManager.getManager();
        YesNoCancelDialogSingleton optionDialog = YesNoCancelDialogSingleton.getSingleton();
        optionDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),  propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        return optionDialog.getSelection();
        //return optionDialog.getSelection().equals(YesNoCancelDialogSingleton.YES);
        //*/
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {

    }

    private boolean illegalKey(char input){
        return ILLEGAL_KEYS.matcher(Character.toString(input)).find();
    }
}
