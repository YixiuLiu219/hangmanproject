package gui;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * Created by Yixiu Liu on 10/9/2016.
 *
 * @author Yixiu Liu
 */
public class HangmanGraphics {
    private final int maxSteps = 10;
    private int currentStep = 1;
    private final Canvas canvas;
    private final GraphicsContext gc;
    private final double relativeX = 0;
    private final double relativeY = 0;
    private final double hangerWidth = 200;
    private final double hangerHeight = 600;
    private final double strokeWidth = 10;
    private final Limb[] limbs = {
            //---
            gc -> {
                gc.setLineWidth(strokeWidth);
                gc.strokeLine(  relativeX, relativeY+hangerHeight-strokeWidth,
                                relativeX+hangerWidth, relativeY+hangerHeight-strokeWidth
                );
            },
            //|
            gc -> {
                gc.setLineWidth(strokeWidth);
                gc.strokeLine(  relativeX+(1/4)*(hangerWidth), relativeY,
                        relativeX+(1/4)*(hangerWidth), relativeY+hangerHeight
                );
            },
            //---
            gc -> {
                gc.setLineWidth(strokeWidth);
                gc.strokeLine(  relativeX, relativeY,
                        relativeX+hangerWidth/2, relativeY
                );
            },
            //|
            gc -> {
                gc.setLineWidth(strokeWidth);
                gc.strokeLine(  relativeX+hangerWidth/2, relativeY,
                        relativeX+hangerWidth/2, relativeY+hangerHeight/5
                );
            },
            //head
            gc -> {
                double radius = hangerHeight/8;
                gc.setLineWidth(strokeWidth);
                gc.strokeOval(relativeX+hangerWidth/2-radius/2, relativeY+hangerHeight/5,
                        radius, radius
                );
            },
            //spine
            gc -> {
                gc.setLineWidth(strokeWidth);
                gc.strokeLine(  relativeX+hangerWidth/2, relativeY+hangerHeight/5+hangerHeight/8,
                        relativeX+hangerWidth/2, relativeY+hangerHeight/2
                );
            },
            //left hand
            gc -> {
                gc.setLineWidth(strokeWidth);
                gc.strokeLine(  relativeX+hangerWidth/2, relativeY+hangerHeight/5+hangerHeight/8,
                        relativeX+hangerWidth/4, relativeY+hangerHeight/2
                );
            },
            //right hand
            gc -> {
                gc.setLineWidth(strokeWidth);
                gc.strokeLine(  relativeX+hangerWidth/2, relativeY+hangerHeight/5+hangerHeight/8,
                        relativeX+hangerWidth*3/4, relativeY+hangerHeight/2
                );
            },
            //left leg
            gc -> {
                gc.setLineWidth(strokeWidth);
                gc.strokeLine(  relativeX+hangerWidth/2, relativeY+hangerHeight/2,
                        relativeX+hangerWidth/4, relativeY+hangerHeight/2+relativeY+hangerHeight/8+hangerHeight/4
                );
            },
            //right leg
            gc -> {
                gc.setLineWidth(strokeWidth);
                gc.strokeLine(  relativeX+hangerWidth/2, relativeY+hangerHeight/2,
                        relativeX+hangerWidth*3/4, relativeY+hangerHeight/2+relativeY+hangerHeight/8+hangerHeight/4
                );
            },
    };

    public HangmanGraphics(){
        canvas = new Canvas(200, 600);
        //canvas.setLayoutX(50);
        //canvas.setLayoutY(0);
        gc = canvas.getGraphicsContext2D();
    }

    public void clearCanvas(){
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    public void step(){
        if(currentStep<=maxSteps){
            limbs[currentStep-1].draw(gc);
            currentStep++;
        }
    }

    public void setStep(int step){//no such thing as step 0
        clearCanvas();
        if(step<=maxSteps) {
            for (int i = 0; i <= step-1; i++)
                limbs[i].draw(gc);
            currentStep = step;
        }
    }

    public Canvas getCanvas(){
        return canvas;
    }

    private interface Limb{
        void draw(GraphicsContext gc);
    }
}
