package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee, Yixiu Liu
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate appTemplate; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label      guiHeadingLabel;   // workspace (GUI) heading label
    HBox       headPane;          // conatainer to display the heading
    HBox       bodyPane;          // container for the main game displays
    ToolBar    footToolbar;       // toolbar for game buttons
    BorderPane figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox       gameTextsPane;     // container to display the text-related parts of the game
    HBox       guessedLetters;    // text area displaying all the letters guessed so far
    HBox       remainingGuessBox; // container to display the number of remaining guesses
    Button     startGame;         // the button to start playing a game of Hangman

    FlowPane       alreadyGuessedBox;
    HangmanGraphics hangmanGraphics;

    private final double maxGuessBoxWidth = 400;


    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        appTemplate = initApp;
        gui = initApp.getGUI();
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();

        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);

        //HangmanGraphics
        hangmanGraphics = new HangmanGraphics();
        figurePane.setCenter(hangmanGraphics.getCanvas());
        figurePane.setStyle("-fx-padding: 20px");

        //Already guessed Pane
        alreadyGuessedBox = new FlowPane();
        alreadyGuessedBox.setMaxWidth(maxGuessBoxWidth);
        gameTextsPane.getChildren().add(alreadyGuessedBox);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
    }

    private void setupHandlers() {
        //HangmanController controller = new HangmanController(appTemplate, startGame);

        HangmanController controller = (HangmanController) appTemplate.getGUI().getController();
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        reinitialize();
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public Pane getAlreadyGuessedBox(){ return alreadyGuessedBox; }

    public HangmanGraphics getHangmanGraphics(){
        return hangmanGraphics;
    }

    public void updateHangmanGraphics(int badguesses){
        hangmanGraphics.setStep(badguesses);
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        //guessedLetters.setPadding(new Insets);
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();

        //HangmanGraphics
        hangmanGraphics.clearCanvas();

        //AlreadyguessBox
        alreadyGuessedBox = new FlowPane();
        alreadyGuessedBox.setMaxWidth(maxGuessBoxWidth);

        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, alreadyGuessedBox);
        //gameTextsPane.getChildren().setAll(remainingGuessBox, stackPane);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }

    public HBox getLetterPane(){
        return guessedLetters;
    }

    public StackPane makePad(Text text, Color color){
        double padding = 10;
        double paneInset = 5;
        double textSize = text.getFont().getSize();

        Rectangle rect = new Rectangle();
        StackPane pane = new StackPane();

        //Format
        pane.setPadding(new Insets(paneInset));
        rect.xProperty().bind(text.xProperty().subtract(padding));
        rect.yProperty().bind(text.yProperty().subtract(padding));
        rect.setWidth(2*padding+textSize);
        rect.setHeight(2*padding+textSize);

        //Customization
        rect.setFill(color);
        rect.setArcWidth(30);
        rect.setArcHeight(30);

        //finalize
        pane.getChildren().addAll(rect, text);
        return pane;
    }

    public Rectangle makePad(Text text, Color color, StackPane stackPane, Pane box){
        double padding = 10;
        double paneInset = 5;
        double textSize = text.getFont().getSize();

        Rectangle rect = new Rectangle();

        //Format
        stackPane.setPadding(new Insets(paneInset));
        rect.xProperty().bind(text.xProperty().subtract(padding));
        rect.yProperty().bind(text.yProperty().subtract(padding));
        rect.setWidth(2*padding+textSize);
        rect.setHeight(2*padding+textSize);

        //Customization
        rect.setFill(color);
        rect.setArcWidth(30);
        rect.setArcHeight(30);

        //finalize
        stackPane.getChildren().addAll(rect, text);
        box.getChildren().add(stackPane);
        return rect;
    }
}
